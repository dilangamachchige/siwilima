<%-- 
    Document   : login
    Created on : Dec 20, 2015, 8:02:07 PM
    Author     : ujith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    
<%@include file="header.jsp"%>
<title>Login - forget password</title>
  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
		      <form class="form-login" action="./forgetPasswordEmail">
                          <h2 class="form-login-heading" >Forget Password</h2>
                          <div style="padding: 5px;">
                              <h5><b>Did you forget your password ?</b></h5>
                          <h6>Don't worry, You can reset password with your email.</h6>
                          </div>
                          <center>
                              <h4 class="label label-danger" style="margin-top: 10px;">${errorM}</h4>
                          </center>
		        <div class="login-wrap">
                            <input type="email" name="user" class="form-control" placeholder="Enter Email" autofocus>
                            <br>
                            <input class="btn btn-theme btn-block"  type="submit" value=" Send reset code"/>
		      </div>
		      </form>	
                </div>
	  </div>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
  <script src="https://raw.githubusercontent.com/srobbin/jquery-backstretch/master/jquery.backstretch.min.js"></script>
 <script>
        $.backstretch("assets/img/4.jpg", {speed: 500});
    </script>
  </body>
</html>

