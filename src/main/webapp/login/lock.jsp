<%-- 
    Document   : login
    Created on : Dec 20, 2015, 8:02:07 PM
    Author     : ujith
--%>

<%@page import="projos.Officeworker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <%@include file="header.jsp"%>
    <%Officeworker k = (Officeworker) session.getAttribute("user"); 
    %>
    <title>Login</title>
    <body>
        <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <div id="login-page">
            <div class="container">
                <form class="form-login" action="./loginFun">
                    <h2 class="form-login-heading" id="jk" >Lock Screen</h2>
                    <center>
                        <h4 class="label label-danger" style="margin-top: 10px;">${errorM}</h4>
                    </center>
                    <div class="login-wrap">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-lg-5">
                                <div style="width: 100px; height: 100px; background-size: 250px; padding: 2px; border: solid 2px #eeeeee;  background-position: -50px -40px; border-radius: 100%; background-image: url(assets/img/<%=k.getUserPic()%>)"></div>
                            </div>
                            <div class="col-lg-7">
                                <h4><%=k.getFname()+" "+k.getLname()%></h4>
                                <input type="hidden" name="user" value="<%=k.getEmail()%>"/>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            </br>    
                        </div>
                        <input class="btn btn-theme btn-block" type="submit" value="Unlock"/>
                    </div>
                </form>	
            </div>
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="https://raw.githubusercontent.com/srobbin/jquery-backstretch/master/jquery.backstretch.min.js"></script>
        <script>
            $.backstretch("assets/img/4.jpg", {speed: 500});
        </script>
    </body>
</html>

