<%-- 
    Document   : login
    Created on : Dec 20, 2015, 8:02:07 PM
    Author     : ujith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
 <%@ taglib prefix="s" uri="/struts-tags" %>   
<%@include file="header.jsp"%>
<title>Login</title>
  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" action="./loginFun">
                          <h2 class="form-login-heading" id="jk" >sign in</h2>
                          <center>
                              <h4 class="label label-danger" style="margin-top: 10px;">${errorM}</h4>
                          </center>
		        <div class="login-wrap">
                            <input type="email" name="user" class="form-control" placeholder="Email" autofocus>
		            <br>
                            <input type="password" name="password" class="form-control" placeholder="Password">
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal"  href="./forgetPassword"> Forgot Password?</a>
		
		                </span>
		            </label>
                            <input class="btn btn-theme btn-block"  type="submit" value="SIGN IN"/>
		            
		        </div>
		
		      </form>	
                
                    
	  	
	  	</div>
	  </div>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
  <script src="https://raw.githubusercontent.com/srobbin/jquery-backstretch/master/jquery.backstretch.min.js"></script>
 <script>
        $.backstretch("assets/img/4.jpg", {speed: 500});
    </script>
  </body>
</html>

