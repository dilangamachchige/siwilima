      <%@page import="projos.Officeworker"%>
<!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <%
      Officeworker worker = (Officeworker)session.getAttribute("user");
      
      %>
      
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" style="color: #AEB2B7;" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="User" class="logo"><b>SIWILIMA - admin</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" style="color: #AEB2B7;" href="#">
                            <i class="fa fa-bell"></i>
                            <span class="badge bg-theme">4</span>
                        </a>
                        <ul class="dropdown-menu extended tasks-bar" >
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green">You have 4 pending tasks</p>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div class="desc">Message description</div>
                                        
                                    </div>
                                    
                                </a>
                            </li>
                            <li class="external">
                                <a href="#">See All Tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                    <li id="header_inbox_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" style="color: #AEB2B7;" href="index.html#">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge bg-theme">5</span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green">You have 5 new messages</p>
                            </li>
                      
                            <li>
                                <a href="#">
                                    <span class="photo"><img alt="avatar" src="../assets/img/friends/fr-05.jpg"></span>
                                    <span class="subject">
                                    <span class="from">From</span>
                                    <span class="time">4 hrs.</span>
                                    </span>
                                    <span class="message">
                                        Message body
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">See all messages</a>
                            </li>
                        </ul>
                    </li>
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="./logout">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="#"><img src="assets/img/<%=worker.getUserPic()%>" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><%=worker.getFname()+" "+worker.getLname()%></h5>
              	  	
                  <li class="mt">
                      <a class="active" href="#">
                          <i class="fa fa-dashboard"></i>
                          <span>Home</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-users"></i>
                          <span>Manage Office Workers</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Add New Worker</a></li>
                          <li><a  href="#">View Workers</a></li>
                          <li><a  href="#">Deleted Workers</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-empire"></i>
                          <span>Manage labourer</span>
                      </a>
                      <ul class="sub">
                         <li><a  href="#">Add New labourer</a></li>
                          <li><a  href="#">View labourer</a></li>
                          <li><a  href="#">Deleted labourer</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Manage Product</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Add New Product</a></li>
                          <li><a  href="#">View All products</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Manage Service</span>
                      </a>
                      <ul class="sub">
                           <li><a  href="#">Add New Service</a></li>
                          <li><a  href="#">View All Service</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Manage Materials</span>
                      </a>
                      <ul class="sub">
                           <li><a  href="#">Add New Materials</a></li>
                          <li><a  href="#">View All Materials</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class=" fa fa-bar-chart-o"></i>
                          <span>Manage Stock</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="morris.html">View All Stock</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-home"></i>
                          <span>Manage Show Room</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Add New Show Room</a></li>
                          <li><a  href="#">View Show Room</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-user"></i>
                          <span>Manage Customers</span>
                      </a>
                      <ul class="sub">
                         <li><a  href="#">Add New Customer</a></li>
                          <li><a  href="#">View Customers</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-scissors"></i>
                          <span>Manage Special Equipment</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Add New Special Equipment</a></li>
                          <li><a  href="#">View Special Equipment</a></li>
                          <li><a  href="#">View with gantt chart</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      