<%-- 
    Document   : home.jsp
    Created on : Dec 22, 2015, 10:42:16 PM
    Author     : ujith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <%@include file="header.jsp"%>
    <title>Admin - Home</title>
    <body>

        <section id="container" >
            <%@include file="headerSideBar.jsp"%>
            <!-- **********************************************************************************************************************************************************
            MAIN CONTENT
            *********************************************************************************************************************************************************** -->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row" style="padding-top: 20px;">

                        <div class="col-md-8 ">
                            <!-- WHITE PANEL - TOP USER -->
                            <div class="white-panel pn">
                                <div class="white-header">
                                    <h5>Show Room Mapping</h5>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3" style="border-right: solid 2px #f4f4f4">
                                        <a class="btn btn-default">
                                            <center><div style="width: 50px; margin: 10px; padding: 10px; height: 50px; border-radius: 100%; background-color: #ed5565">
                                            <i class="fa fa-user" style="font-size: 37px;color: #FFF"></i>
                                            </div></center>
                                        <h5 class="centered">Super Admin</h5>
                                        </a>
                                    </div>
                                    <div class="col-lg-9">

                                        
                                        <div class="row">
                                         <div class="row">
                                            <div class="col-lg-3" style="border-right: solid 2px #f4f4f4">
                                                <a class="btn btn-info">
                                                     <center><div style="width: 50px; margin: 10px; padding: 10px; height: 50px; border-radius: 100%; background-color: #4fc1e9">
                                            <i class="fa fa-user" style="font-size: 37px;color: #FFF"></i>
                                            </div></center>
                                                <h5 class="centered">ShowRoom Owner</h5>
                                                </a>
                                            </div>
                                        </div>
                                            <div class="row" style="margin-bottom: 10px;">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-9">
                                                    <div class="row">
                                                        <div class="col-lg-3" style="border-right: solid 2px #f4f4f4; margin: 5px;">
                                                    <a class="btn btn-default">
                                                        <center><div style="width: 50px; margin: 10px; padding: 10px; height: 50px; border-radius: 100%; background-color: #68dff0">
                                            <i class="fa fa-home" style="font-size: 29px;color: #FFF"></i>
                                            </div></center>
                                                    <h5 class="centered">Show Room</h5>
                                                    </a>
                                                </div>
                                                        
                                                        <div class="col-lg-3" style="border-right: solid 2px #f4f4f4; margin: 5px;">
                                                    <a class="btn btn-default">
                                                        <center><div style="width: 50px; margin: 10px; padding: 10px; height: 50px; border-radius: 100%; background-color: #68dff0">
                                            <i class="fa fa-home" style="font-size: 29px;color: #FFF"></i>
                                            </div></center>
                                                    <h5 class="centered">Show Room</h5>
                                                    </a>
                                                </div>
                                                        
                                                           <div class="col-lg-3" style="border-right: solid 2px #f4f4f4; margin: 5px;">
                                                    <a class="btn btn-default">
                                                        <center><div style="width: 50px; margin: 10px; padding: 10px; height: 50px; border-radius: 100%; background-color: #68dff0">
                                            <i class="fa fa-home" style="font-size: 29px;color: #FFF"></i>
                                            </div></center>
                                                    <h5 class="centered">Show Room</h5>
                                                    </a>
                                                </div>
                                                        
                                                    </div>
                                            </div>
                                            </div>
                                        </div>
                              
                                       
                                         
                                    </div>
                                </div>
                            </div>
                        </div><!-- /col-md-4 -->

                    </div>
                </section>
            </section>

            <!--main content end-->
            <%@include file="footer.jsp"%>  
        </section>
        <%@include file="foot.jsp"%>





    </body>
</html>
