package projos;
// Generated Dec 20, 2015 7:57:53 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * SubShowRoom generated by hbm2java
 */
public class SubShowRoom  implements java.io.Serializable {


     private Integer idsubShowRoom;
     private Officeworker officeworker;
     private ShowRoom showRoom;
     private Double lat;
     private Double lng;
     private Date date;
     private Integer status;
     private Set stockProducts = new HashSet(0);
     private Set customerses = new HashSet(0);
     private Set showroomLabourers = new HashSet(0);
     private Set showroomMemberses = new HashSet(0);

    public SubShowRoom() {
    }

	
    public SubShowRoom(Officeworker officeworker, ShowRoom showRoom) {
        this.officeworker = officeworker;
        this.showRoom = showRoom;
    }
    public SubShowRoom(Officeworker officeworker, ShowRoom showRoom, Double lat, Double lng, Date date, Integer status, Set stockProducts, Set customerses, Set showroomLabourers, Set showroomMemberses) {
       this.officeworker = officeworker;
       this.showRoom = showRoom;
       this.lat = lat;
       this.lng = lng;
       this.date = date;
       this.status = status;
       this.stockProducts = stockProducts;
       this.customerses = customerses;
       this.showroomLabourers = showroomLabourers;
       this.showroomMemberses = showroomMemberses;
    }
   
    public Integer getIdsubShowRoom() {
        return this.idsubShowRoom;
    }
    
    public void setIdsubShowRoom(Integer idsubShowRoom) {
        this.idsubShowRoom = idsubShowRoom;
    }
    public Officeworker getOfficeworker() {
        return this.officeworker;
    }
    
    public void setOfficeworker(Officeworker officeworker) {
        this.officeworker = officeworker;
    }
    public ShowRoom getShowRoom() {
        return this.showRoom;
    }
    
    public void setShowRoom(ShowRoom showRoom) {
        this.showRoom = showRoom;
    }
    public Double getLat() {
        return this.lat;
    }
    
    public void setLat(Double lat) {
        this.lat = lat;
    }
    public Double getLng() {
        return this.lng;
    }
    
    public void setLng(Double lng) {
        this.lng = lng;
    }
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Set getStockProducts() {
        return this.stockProducts;
    }
    
    public void setStockProducts(Set stockProducts) {
        this.stockProducts = stockProducts;
    }
    public Set getCustomerses() {
        return this.customerses;
    }
    
    public void setCustomerses(Set customerses) {
        this.customerses = customerses;
    }
    public Set getShowroomLabourers() {
        return this.showroomLabourers;
    }
    
    public void setShowroomLabourers(Set showroomLabourers) {
        this.showroomLabourers = showroomLabourers;
    }
    public Set getShowroomMemberses() {
        return this.showroomMemberses;
    }
    
    public void setShowroomMemberses(Set showroomMemberses) {
        this.showroomMemberses = showroomMemberses;
    }




}


