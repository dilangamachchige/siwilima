package projos;
// Generated Dec 20, 2015 7:57:53 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Materials generated by hbm2java
 */
public class Materials  implements java.io.Serializable {


     private Integer idmaterials;
     private String name;
     private String pic;
     private Date date;
     private Integer status;
     private Set matSellingprices = new HashSet(0);

    public Materials() {
    }

    public Materials(String name, String pic, Date date, Integer status, Set matSellingprices) {
       this.name = name;
       this.pic = pic;
       this.date = date;
       this.status = status;
       this.matSellingprices = matSellingprices;
    }
   
    public Integer getIdmaterials() {
        return this.idmaterials;
    }
    
    public void setIdmaterials(Integer idmaterials) {
        this.idmaterials = idmaterials;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getPic() {
        return this.pic;
    }
    
    public void setPic(String pic) {
        this.pic = pic;
    }
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Set getMatSellingprices() {
        return this.matSellingprices;
    }
    
    public void setMatSellingprices(Set matSellingprices) {
        this.matSellingprices = matSellingprices;
    }




}


