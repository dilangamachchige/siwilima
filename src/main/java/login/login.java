/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import com.opensymphony.xwork2.ActionSupport;
import com.sun.xml.ws.util.StringUtils;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import projos.Officeworker;
import projos.UserPassword;

/**
 *
 * @author ujith
 */
public class login extends ActionSupport {

    private String keyp;

    public login() {
    }

    HttpSession hs;
    private String user;
    private String password;
    private String errorM;
    private String key;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        hs = request.getSession();
        if (hs.getAttribute("user") == null) {
            if (errorM == null || errorM.equals("")) {
                return "log";
            } else {
                return "log";
            }
        } else {
            return "Session";
        }
    }

    public String forgetpass() throws Exception {
        return "log";
    }

    public String forgetEmail() throws Exception {
        if (!user.equals("") || user != null || org.apache.commons.lang3.StringUtils.isEmpty(user) ) {
            Session l = db.util.getSessionFactory().openSession();
            Criteria cr = l.createCriteria(Officeworker.class);
            cr.add(Restrictions.eq("email", user));
            if (cr.list().size() > 0) {
                Officeworker c = (Officeworker) cr.list().get(0);
                if (c.getStatus() != 0) {
                    String msg = "";
                    Date d = new Date();
                    long h = d.getTime() + (1000 * 60 * 5);
                    msg = "This is an automated email, please do not reply	\n"
                            + "\n"
                            + "Dear " + c.getFname() + " " + c.getLname() + ",\n"
                            + "\n"
                            + "    \n"
                            + "Resettig forgot password\n"
                            + "\n"
                            + "You  have 5 min to get this link and do your reset process.\n"
                            + "http://localhost:8080/siwilima/ResetPassword?key=" + h + "," + c.getIdofficeWorker() + " \n"
                            + "\n"
                            + "Please take this time to explore our website and discover everything we have to offer. If you need more information about our fees or if you have any questions, check out our comprehensive FAQs or contact our awesome Customer Support!\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "Need Assistance?\n"
                            + "We're happy to help by Live Chat or by email Monday to Friday 24 hours/day.\n"
                            + "\n"
                            + "Copyright 2015 SIWILIMA. All rights reserved.\n"
                            + "";
                    email.senMail.sendMail(msg, c.getEmail(), "Forget Password SIWILIMA Admin");
                    l.flush();
                    l.close();
                    errorM = "Reset code was sent.";
                    return SUCCESS;
                } else {
                    errorM = "Selected user deactivated.";
                    return ERROR;
                }
            } else {
                errorM = "No user Detected.";
                return ERROR;
            }
        } else {
            errorM = "Enter valid email address.";
            return ERROR;
        }
    }

    public String ResetPassword() throws Exception {
        if (key == null  || key.equals("") || org.apache.commons.lang3.StringUtils.isEmpty(key)) {
            errorM = "No resetting key found.";
            return ERROR;
        } else {
            keyp = key.split(",")[0];
            Date d = new Date();
            long j = Long.parseLong(keyp);
            if (!(j - d.getTime() > 0)) {
                errorM = "Reset code expired. Try with new one.";
                return ERROR;
            } else {
                HttpServletRequest request = ServletActionContext.getRequest();
                hs = request.getSession();
                hs.setAttribute("resetPass", key);
                return SUCCESS;
            }
        }
    }

    public String resetFunction() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        hs = request.getSession();
        if (hs.getAttribute("resetPass") == null || password == null|| password.equals("")  || user == null|| user.equals("") ) {
            errorM = "Enter valid details. Try with new one.";
            return ERROR;
        } else {
            if (password.equals(user) && password.length() > 5) {
                Session openSession = db.util.getSessionFactory().openSession();
                Criteria createCriteria = openSession.createCriteria(UserPassword.class);
                Officeworker worker = new Officeworker();
                String keyd = (String) hs.getAttribute("resetPass");
                System.out.println(keyd);
                worker.setIdofficeWorker(Integer.parseInt(keyd.split(",")[1]));
                createCriteria.add(Restrictions.eq("officeworker", worker));
                Transaction beginTransaction = openSession.beginTransaction();
                List list = createCriteria.list();
                for (int i = 0; i < list.size(); i++) {
                    UserPassword get = (UserPassword) list.get(i);
                    get.setStatus(0);
                    openSession.update(get);
                }
                UserPassword newuserPassword = new UserPassword(worker, user, new Date(), 1);
                openSession.save(newuserPassword);
                beginTransaction.commit();;
                openSession.flush();
                openSession.close();
                hs.removeAttribute("resetPass");
                errorM = "Password resetted.";
                return SUCCESS;
            } else {
                errorM = "password too short. Try with new key.";
                return ERROR;
            }
        }
    }

    public String logout() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        hs = request.getSession();
        hs.removeAttribute("user");
        errorM = "successfully loging out.";
        return "log";
    }

    public String lock() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        hs = request.getSession();
        if (hs.getAttribute("user") != null) {       
                return "log";    
        } else {
            return "Session";
        }
    }

    public String loginFun() throws Exception {
        //String logout = logout();
        if (user == null || user.equals("") || password == null || password.equals("") || org.apache.commons.lang3.StringUtils.isEmpty(user) || org.apache.commons.lang3.StringUtils.isEmpty(password)) {
            errorM = "Enter valid details.";
            return ERROR;
        } else {
            Session openSession = db.util.getSessionFactory().openSession();
            Criteria createCriteria = openSession.createCriteria(Officeworker.class);
            createCriteria.add(Restrictions.eq("email", user));
            createCriteria.add(Restrictions.eq("status", 1));
            if (createCriteria.list().size() > 0) {
                Officeworker get = (Officeworker) createCriteria.list().get(0);
                Criteria createCriteria1 = openSession.createCriteria(UserPassword.class);
                Criterion status = Restrictions.eq("status", 1);
                Criterion id = Restrictions.eq("officeworker", get);
                LogicalExpression log = Restrictions.and(status, id);
                createCriteria1.add(log);
                List list = createCriteria1.list();
                System.out.println("size - " + list.size());
                UserPassword get1 = (UserPassword) list.get(0);
                if (get1.getPassword().equals(password)) {
                    HttpServletRequest request = ServletActionContext.getRequest();
                    hs = request.getSession();
                    hs.setAttribute("user", get);
                    openSession.flush();
                    openSession.close();
                    return SUCCESS;
                } else {
                    openSession.flush();
                    openSession.close();
                    errorM = "Wrong password.";
                    return ERROR;
                }
            } else {
                openSession.flush();
                openSession.close();
                errorM = "No user detected.";
                return ERROR;
            }
        }
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the errorM
     */
    public String getErrorM() {
        return errorM;
    }

    /**
     * @param errorM the errorM to set
     */
    public void setErrorM(String errorM) {
        this.errorM = errorM;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }
}
