package user;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author ujith
 */
public class user extends ActionSupport {
    private HttpSession hs;
    
    public user() {
    }
    
    public String execute() throws Exception {
         HttpServletRequest request = ServletActionContext.getRequest();
        hs = request.getSession();
        if(hs.getAttribute("user")!=null)
        return "log";
        else
        return ERROR;
    }
    
}
